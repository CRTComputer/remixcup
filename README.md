# <div align="center">RemixCup</div>

![](https://img.shields.io/badge/Open%20Source-❤-blue)
![](https://img.shields.io/badge/GNU%20GPL-v3.0-red)
![](https://img.shields.io/badge/Version-Not%20released%20yet-brightgreen)
<div align="center"><b>A YouTube Client that is privacy oriented and has AI recommendations</b></div>

## Why not Newpipe?

Sure, Newpipe is good and all but there is one feeling I missed using Newpipe is because of recommendations. Why recommendations? It is because you will find a new YouTube channel that has the same content and still be a new channel you can explore and watch about. Newpipe doesn't do any of that. That's one of the reasons I made this project.

## Privacy Policy

When you start the app you will be required to select if you want to opt-in the "Improved Experience" feature which will enable AI Recommendation, Video History and Subscribe, if you want to you can disable features if you don't like it.

Using AI recommendation requires this app to log what video you're watching (Video ID, Title and it's tags only) and what you're searching.

Using Video History requires this app to log what video you're watching (Video ID, Title, Channel name only).

Using Subscribe requires the app to log what channels you're subscribed to, which you can use it to enable push notifications, you can enable it during setup or in settings.

Internet is only for reciving YouTube/Invidious' data, no uploading you're data ever, and NO APIs. You can opt-in or opt-out how many times you want. (If you don't trust me you can check the source code)

## Credits

TeamNewPipe for some code needed to get Downloader working

### Third Party Licenses


## Features

### User experience

* AI recommendation
* No sending your data to the cloud

### User Interface

* Looks like stock YouTube

## To-dos
- [ ] Home
    - [ ] AI Recommendation
        - [ ] Video RecyclerView
            - [ ] RecyclerView
            - [ ] Import AI data to RecyclerView
        - [ ] Topics
    - [ ] Recent View History

- [ ] Explore
    - [ ] Video RecyclerView
        - [x] RecyclerView
        - [ ] Import Trending to RecyclerView
    - [ ] Channel Topics (Gaming, Music, etc.)

- [ ] Subscriptions
    - [ ] Import Subscriptions
    - [ ] Import Latest Videos

- [ ] Notifications
    - [ ] Import Subscriptions
    - [ ] Push Notifications
        - [ ] Video Check

- [ ] Library
    - [ ] Video History
    - [ ] Saved Playlists

- [ ] Video Player
    - [ ] Player
    - [ ] Comments
    - [ ] AI Recommendations
        - [ ] Video RecyclerView
            - [ ] Import data from AI

- [ ] Settings