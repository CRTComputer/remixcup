/*
 *     RemixCup -  A privacy-oriented YouTube Client that has AI Recommendations.
 *     Copyright (C) 2020-2020  CRTComputer
 *
 *     RemixCup is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     RemixCup is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with RemixCup.  If not, see <https://www.gnu.org/licenses/>.
 */

package crtcomputer.remixcup.data.network

import crtcomputer.remixcup.data.database.entity.ExploreVideoData
import okhttp3.OkHttpClient
import org.schabi.newpipe.DownloaderImpl
import org.schabi.newpipe.extractor.NewPipe
import org.schabi.newpipe.extractor.ServiceList
import org.schabi.newpipe.extractor.downloader.Downloader
import org.schabi.newpipe.extractor.feed.FeedInfo
import org.schabi.newpipe.extractor.localization.Localization

interface TrendingNetwork {
    fun getTrendingData() {
        val videoList = ArrayList<ExploreVideoData>()

        try {
            DownloaderImpl.init(OkHttpClient.Builder())
            NewPipe.init(DownloaderImpl.getInstance(), Localization("GB", "en"))
            NewPipe.getDownloader().get("https://youtube.com/feed/trending").responseBody()

        } catch (e: Exception) {

        }
    }
}