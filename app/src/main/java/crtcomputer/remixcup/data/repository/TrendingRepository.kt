/*
 *     RemixCup -  A privacy-oriented YouTube Client that has AI Recommendations.
 *     Copyright (C) 2020-2020  CRTComputer
 *
 *     RemixCup is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     RemixCup is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with RemixCup.  If not, see <https://www.gnu.org/licenses/>.
 */

package crtcomputer.remixcup.data.repository

import crtcomputer.remixcup.data.database.TrendingDatabase
import crtcomputer.remixcup.data.database.entity.ExploreVideoData
import crtcomputer.remixcup.data.network.TrendingNetwork
import okhttp3.OkHttpClient
import org.schabi.newpipe.DownloaderImpl
import org.schabi.newpipe.extractor.NewPipe
import org.schabi.newpipe.extractor.ServiceList
import org.schabi.newpipe.extractor.feed.FeedInfo
import org.schabi.newpipe.extractor.localization.Localization

class TrendingRepository(
    private val database: TrendingDatabase,
) {
    suspend fun upsert(trending_video: ExploreVideoData) = database.trendingDao().upsert(trending_video)

    suspend fun delete(trending_video: ExploreVideoData) = database.trendingDao().delete(trending_video)

    fun getTrendingVideos() = database.trendingDao().getTrendingVideos()

}