/*
 *     RemixCup -  A privacy-oriented YouTube Client that has AI Recommendations.
 *     Copyright (C) 2020-2020  CRTComputer
 *
 *     RemixCup is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     RemixCup is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with RemixCup.  If not, see <https://www.gnu.org/licenses/>.
 */

package crtcomputer.remixcup.data.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "trending_video_table")
data class ExploreVideoData(
        @PrimaryKey(autoGenerate = false)
        val videoID: String,
        @ColumnInfo(name = "trending_video_videoID")
        val videoURL: String,
        @ColumnInfo(name = "trending_video_title")
        val title: String,
        @ColumnInfo(name = "trending_video_views")
        val views: Long,
        @ColumnInfo(name = "trending_video_channel")
        val channel: String,
        @ColumnInfo(name = "trending_video_channelImg")
        val channelImg: String,
        @ColumnInfo(name = "trending_video_videoDate")
        val videoDate: String?,
        @ColumnInfo(name = "trending_video_thumbnail")
        val thumbnail: String,
        @ColumnInfo(name = "trending_video_length")
        val length: Long
)