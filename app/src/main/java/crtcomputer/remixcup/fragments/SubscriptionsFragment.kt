package crtcomputer.remixcup.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import crtcomputer.remixcup.databinding.FragmentSubscriptionsBinding

class SubscriptionsFragment : Fragment() {

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentSubscriptionsBinding = FragmentSubscriptionsBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

}