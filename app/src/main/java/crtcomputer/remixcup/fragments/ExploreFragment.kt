/*
 *     RemixCup -  A privacy-oriented YouTube Client that has AI Recommendations.
 *     Copyright (C) 2020-2020  CRTComputer
 *
 *     RemixCup is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     RemixCup is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with RemixCup.  If not, see <https://www.gnu.org/licenses/>.
 */

package crtcomputer.remixcup.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import crtcomputer.remixcup.adapters.RecyclerExploreAdapter
import crtcomputer.remixcup.data.database.TrendingDatabase
import crtcomputer.remixcup.data.database.entity.ExploreVideoData
import crtcomputer.remixcup.data.repository.TrendingRepository
import crtcomputer.remixcup.databinding.FragmentExploreBinding
import crtcomputer.remixcup.fragments.viewmodel.ExploreViewModel
import crtcomputer.remixcup.fragments.viewmodel.ExploreViewModelFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import org.schabi.newpipe.DownloaderImpl
import org.schabi.newpipe.extractor.NewPipe
import org.schabi.newpipe.extractor.ServiceList
import org.schabi.newpipe.extractor.localization.Localization

class ExploreFragment : Fragment() {

    private lateinit var binding: FragmentExploreBinding
    private var videoList = ArrayList<ExploreVideoData>()

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        /* Bind the view */
        binding =
                FragmentExploreBinding.inflate(layoutInflater, container, false)

        val database = TrendingDatabase(requireContext())
        val repository = TrendingRepository(database)
        val factory = ExploreViewModelFactory(repository)

        val viewModel = ViewModelProvider(this, factory).get(ExploreViewModel::class.java)

        val adapter = RecyclerExploreAdapter(arrayListOf())

        val recyclerExploreAdapter = RecyclerExploreAdapter(videoList)
        val linearLayoutManager = LinearLayoutManager(context)
        val recyclerView = binding.exploreRecyclerview

        recyclerView.layoutManager = linearLayoutManager
        recyclerView.adapter = recyclerExploreAdapter

        CoroutineScope(IO).launch {
            DownloaderImpl.init(OkHttpClient.Builder())
            NewPipe.init(DownloaderImpl.getInstance(), Localization("GB", "en"))
            println(NewPipe.getDownloader()["https://youtube.com/feed/trending"].responseCode())
        }

        return binding.root

    }

}

