/*
 *     RemixCup -  A privacy-oriented YouTube Client that has AI Recommendations.
 *     Copyright (C) 2020-2020  CRTComputer
 *
 *     RemixCup is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     RemixCup is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with RemixCup.  If not, see <https://www.gnu.org/licenses/>.
 */

package crtcomputer.remixcup.fragments.viewmodel

import androidx.lifecycle.ViewModel
import crtcomputer.remixcup.data.database.entity.ExploreVideoData
import crtcomputer.remixcup.data.repository.TrendingRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch

class ExploreViewModel(
    private val repository: TrendingRepository
): ViewModel() {
    fun upsert(trending_video: ExploreVideoData) = CoroutineScope(Main).launch {
        repository.upsert(trending_video)
    }

    fun delete(trending_video: ExploreVideoData) = CoroutineScope(Main).launch {
        repository.delete(trending_video)
    }

    fun getTrendingVideos() = repository.getTrendingVideos()

}