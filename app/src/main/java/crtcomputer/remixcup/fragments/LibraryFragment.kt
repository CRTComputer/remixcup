package crtcomputer.remixcup.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import crtcomputer.remixcup.databinding.FragmentLibraryBinding

class LibraryFragment : Fragment() {

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentLibraryBinding = FragmentLibraryBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

}