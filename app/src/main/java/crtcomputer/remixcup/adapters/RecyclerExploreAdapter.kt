package crtcomputer.remixcup.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import crtcomputer.remixcup.R
import crtcomputer.remixcup.data.database.entity.ExploreVideoData
import crtcomputer.remixcup.databinding.VideocardBinding

class RecyclerExploreAdapter(private var videoData: ArrayList<ExploreVideoData>) :
    RecyclerView.Adapter<RecyclerExploreAdapter.ExploreViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ExploreViewHolder {
        return ExploreViewHolder(
            LayoutInflater.from(p0.context).inflate(R.layout.videocard, p0, false)
        )
    }

    override fun onBindViewHolder(p0: ExploreViewHolder, p1: Int) {
        val video = videoData[p1]
        val binding = VideocardBinding.bind(p0.view)

        binding.videoTitle.text = video.title
        binding.videoViews.text = "${video.views}"
        binding.videoDate.text = "${video.videoDate}"
        Picasso.get().load(video.thumbnail).into(binding.videoImage)
    }

    override fun getItemCount(): Int = videoData.size

    class ExploreViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}